#include "BuildOrder.h"
#include <sstream>

BuildOrderQueue::BuildOrderQueue(PacifistAres &bot)
	: bot(bot) {
}

BuildOrderItem::BuildOrderItem(const sc2::UNIT_TYPEID &unit_type, const sc2::ABILITY_ID &ability, std::string name, typeOfItem type)
	: unit_type(unit_type),
	ability(ability),
	name(name),
	type(type) {

}

void BuildOrderQueue::clear() { b_queue.clear(); }

void BuildOrderQueue::queueItem(const BuildOrderItem &b) { b_queue.push_back(b); }

void BuildOrderQueue::queueItem_Front(const BuildOrderItem &b) { b_queue.push_front(b); }

void BuildOrderQueue::pop() { b_queue.pop_front(); }

size_t BuildOrderQueue::size() { return b_queue.size(); }
size_t BuildOrderQueue::size() const { return b_queue.size(); }

bool BuildOrderQueue::isEmpty() { return (b_queue.size() == 0);  }

std::string BuildOrderQueue::getInfo() const {
	size_t reps = b_queue.size();
	std::stringstream ss;

	for (size_t i(0); i < reps; i++) {
		const std::string &item = b_queue[b_queue.size() - 1 - i].name;
		ss << item << "\n";
	}

	return ss.str();
}

const BuildOrderItem & BuildOrderQueue::operator[] (const size_t &index) const { return b_queue[index]; }
BuildOrderItem & BuildOrderQueue::operator[] (const size_t &index) { return b_queue[index]; }