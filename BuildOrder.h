#pragma once

#include "sc2api/sc2_api.h"

class PacifistAres;


struct BuildOrderItem {
	enum typeOfItem {
		Unit,
		Building,
		Upgrade
	};

	sc2::ABILITY_ID ability;
	sc2::UNIT_TYPEID unit_type;
	std::string name;
	typeOfItem type;
	
	BuildOrderItem(const sc2::UNIT_TYPEID &unit, const sc2::ABILITY_ID &ability, std::string name, typeOfItem type);
};

class BuildOrderQueue {

	PacifistAres &bot;
	std::deque<BuildOrderItem> b_queue;

public :
	BuildOrderQueue(PacifistAres &bot);

	void clear();
	void queueItem(const BuildOrderItem &b);
	void queueItem_Front(const BuildOrderItem &b);
	void pop();

	size_t size();
	size_t size() const;

	bool isEmpty();

	std::string getInfo() const;

	const BuildOrderItem & operator [] (const size_t & index) const;
	BuildOrderItem & operator [] (const size_t & index);


};