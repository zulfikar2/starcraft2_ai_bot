#include "Data.h"

std::tuple<int,int, int> Data::getBuildingCost(const sc2::UNIT_TYPEID &unit) {
	switch (unit) {
		case sc2::UNIT_TYPEID::ZERG_HATCHERY:
			return std::make_tuple(300, 0, 0);
		case sc2::UNIT_TYPEID::ZERG_SPAWNINGPOOL:
			return std::make_tuple(200, 0, 0);
		case sc2::UNIT_TYPEID::ZERG_ROACHWARREN:
			return std::make_tuple(150, 0, 0);
		default:
			return std::make_tuple(-1, -1, -1);
	}
}

std::tuple<int, int, int> Data::getCost(const BuildOrderItem &item) {
	switch (item.type) {
		case BuildOrderItem::typeOfItem::Building:
			return Data::getBuildingCost(item.unit_type);
		case BuildOrderItem::typeOfItem::Unit:
			return Data::getUnitCost(item.unit_type);
		case BuildOrderItem::typeOfItem::Upgrade:
			return Data::getUpgradeCost(item.ability);
		default:
			return std::make_tuple(-1, -1, -1);
	}
}

std::tuple<int, int, int> Data::getUnitCost(const sc2::UNIT_TYPEID &unit) {
	switch (unit) {
		case sc2::UNIT_TYPEID::ZERG_DRONE:
			return std::make_tuple(50, 0, 1);
		case sc2::UNIT_TYPEID::ZERG_OVERLORD:
			return std::make_tuple(100, 0, 0);
		case sc2::UNIT_TYPEID::ZERG_ZERGLING:
			return std::make_tuple(300, 0, 1);
		case sc2::UNIT_TYPEID::ZERG_ROACH:
			return std::make_tuple(200, 0, 2);
		default:
			return std::make_tuple(-1, -1, -1);
	}
}

std::tuple<int, int, int> Data::getUpgradeCost(const sc2::ABILITY_ID &unit_ability) {
	switch (unit_ability) {
		case sc2::ABILITY_ID::RESEARCH_ZERGMELEEWEAPONSLEVEL1:
			return std::make_tuple(100, 100, 0);
		default:
			return std::make_tuple(-1, -1, -1);
	}
}

BuildOrderItem Data::getRequiredItem(sc2::ABILITY_ID &unit_ability) {
	switch (unit_ability) {
		case sc2::ABILITY_ID::TRAIN_ZERGLING:
			return BuildOrderItem(sc2::UNIT_TYPEID::ZERG_SPAWNINGPOOL, sc2::ABILITY_ID::BUILD_SPAWNINGPOOL, "Spawning Pool", BuildOrderItem::typeOfItem::Building);
		case sc2::ABILITY_ID::TRAIN_ROACH:
			return BuildOrderItem(sc2::UNIT_TYPEID::ZERG_ROACHWARREN, sc2::ABILITY_ID::BUILD_ROACHWARREN, "Roach Warren", BuildOrderItem::typeOfItem::Building);
		default:
			return BuildOrderItem(sc2::UNIT_TYPEID::ZERG_HATCHERY, sc2::ABILITY_ID::BUILD_HATCHERY, "Hatchery", BuildOrderItem::typeOfItem::Building);
	}
}