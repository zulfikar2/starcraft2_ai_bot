#pragma once

#include "sc2api/sc2_api.h"
#include "BuildOrder.h"

class Data {
public:
	static std::tuple<int, int, int> getCost(const BuildOrderItem &item);

	static std::tuple<int, int, int> getUnitCost(const sc2::UNIT_TYPEID &unit);
	static std::tuple<int, int, int> getBuildingCost(const sc2::UNIT_TYPEID &unit);
	static std::tuple<int, int, int> getUpgradeCost(const sc2::ABILITY_ID &unit_ability);

	static BuildOrderItem getRequiredItem(sc2::ABILITY_ID &unit_ability);
};