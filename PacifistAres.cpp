#include <iostream>

#include "PacifistAres.h"
#include "sc2api/sc2_api.h"
#include "sc2api/sc2_types.h"

PacifistAres::PacifistAres()
	:pm(*this) {
}


void PacifistAres::OnGameStart() {
	std::cout << "Bot PacifistAres started..." << std::endl;
	pm.onStart();

}
void PacifistAres::OnStep() {
	// What your bot does every game step
	pm.onStep();
	Debug()->SendDebug();

}

void PacifistAres::OnUnitDestroyed(const sc2::Unit *unit) {

}
void PacifistAres::OnUnitEnterVision(const sc2::Unit *unit) {

}

void PacifistAres::OnBuildingConstructionComplete(const sc2::Unit* unit) {

}

void PacifistAres::OnUnitCreated(const sc2::Unit *unit) {

}

void PacifistAres::OnUnitIdle(const sc2::Unit *unit) {
	
}

int PacifistAres::getCurrFrame() const {
	return (int(Observation()->GetGameLoop()));
}

int PacifistAres::getMins() const {
	return (int(Observation()->GetMinerals()));
}

int PacifistAres::getGas() const {
	return (int(Observation()->GetVespene()));
}

int PacifistAres::getSupply() const {
	return (int(Observation()->GetFoodUsed()));
}

int PacifistAres::getMaxSupply() const {
	return (int(Observation()->GetFoodCap()));
}

sc2::Units PacifistAres::getUnits() const {
	return Observation()->GetUnits();
}

sc2::Units PacifistAres::getUnits(sc2::Unit::Alliance ally) const {
	return Observation()->GetUnits(ally);
}

sc2::Units PacifistAres::getUnits(sc2::Unit::Alliance ally, sc2::UNIT_TYPEID unit_type) const {
	return Observation()->GetUnits(ally, sc2::IsUnit(unit_type));
}