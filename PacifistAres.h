#pragma once

#include "sc2api/sc2_interfaces.h"
#include "sc2api/sc2_agent.h"
#include "sc2api/sc2_map_info.h"
#include "sc2lib/sc2_lib.h"
#include "ProductionManager.h"



class PacifistAres : public sc2::Agent
{
public:
	PacifistAres();
	virtual void OnGameStart() override;
	virtual void OnStep() override;
    virtual void OnUnitIdle(const sc2::Unit *unit) override;
	virtual void OnUnitDestroyed(const sc2::Unit *unit) override;
    virtual void OnUnitCreated(const sc2::Unit *unit) override;
    virtual void OnUnitEnterVision(const sc2::Unit *unit) override;
	virtual void OnBuildingConstructionComplete(const sc2::Unit *unit) override;

	int getCurrFrame() const;
	int getMins() const;
	int getGas() const;
	int getSupply() const;
	int getMaxSupply() const;
	sc2::Units PacifistAres::getUnits() const;
	sc2::Units PacifistAres::getUnits(sc2::Unit::Alliance ally) const;
	sc2::Units PacifistAres::getUnits(sc2::Unit::Alliance ally, sc2::UNIT_TYPEID unit_type) const;

private:
	ProductionManager pm;
	
};

