#include "ProductionManager.h"
#include "PacifistAres.h"
#include "Data.h"
#include <iostream>
#include <sstream>
#include <tuple>

ProductionManager::ProductionManager(PacifistAres & bot)
	:bot(bot)
	, b_queue(bot)
	, nextBuildingLocation(-1.0, -1.0){
}

void ProductionManager::setBuildOrder(const BuildOrderQueue &b_queue) {
	this->b_queue.clear();

	for (size_t i(0); i < b_queue.size(); ++i) {
		this->b_queue.queueItem(b_queue[i]);
	}
}

void ProductionManager::create(BuildOrderItem &item) {
	if (canMake(item)) {
		if (item.type == BuildOrderItem::typeOfItem::Building) {
			//check if it's a hatchery or evolution chamber which we can have more than 1 building for
			/*if ((item.unit_type != sc2::UNIT_TYPEID::ZERG_HATCHERY) || (item.unit_type != sc2::UNIT_TYPEID::ZERG_EVOLUTIONCHAMBER)) {
				//if it isn't and we already have one built, then just pop it and return
				if (CountUnitType(item.unit_type) > 0) { b_queue.pop(); return; }
			}*/
			//try to build building, if it passes, pop it off queue
			if (TryBuildStructure(item.ability)) {
				if (b_queue.size() > 0) {
					b_queue.pop(); 
				}
			}

		} else if (item.type == BuildOrderItem::typeOfItem::Unit) {
			//check if enough foodSupply, if not, make overlord first
			if (bot.getSupply() + std::get<2>(Data::getCost(item)) >= bot.getMaxSupply()) {
				Overlord();
			}
			//check if has required pre-req
			sc2::UNIT_TYPEID reqStruct = Data::getRequiredItem(item.ability).unit_type;
			if (hasRequired(reqStruct)) {
				// Check if building is fully built
				if (checkBuilt(reqStruct)) {
					//std::cout << "Has required : " << sc2::UnitTypeToName(reqStruct) << " for : " << item.name << std::endl;
					// Check if enough larva for unit
					if (CountUnitType(sc2::UNIT_TYPEID::ZERG_LARVA) > 0) {
						if (TryBuildUnit(item.ability)) {
							if (b_queue.size() > 0) { b_queue.pop(); }
						}
					}
				}
			} else {
				//re-queue building that's required if not in queue
				std::cout << "Re-Queing : " << Data::getRequiredItem(item.ability).name <<  " For : " << item.name << std::endl;
				b_queue.queueItem_Front(Data::getRequiredItem(item.ability));
			}

		} else if (item.type == BuildOrderItem::typeOfItem::Upgrade) {
			//TODO: call function to make add upgrade create task

		}
	}
}
bool ProductionManager::hasRequired(sc2::UNIT_TYPEID &unit_type) {
	//check if atleast 1 exists
	if (CountUnitType(unit_type) > 0) {
		return true;
	}
	return false;
}

bool ProductionManager::checkBuilt(sc2::UNIT_TYPEID &unit_type) {
	//check if the building is constructed fully
	sc2::Units units = bot.getUnits(sc2::Unit::Alliance::Self, unit_type);
	for (auto &unit : units) {
		//std::cout << "Progress for " << sc2::UnitTypeToName(unit_type) << " : " << unit->build_progress << std::endl;
		if (unit->build_progress == 1.0f) {
			return true;
		}
	}
	return false;
}

bool ProductionManager::canMake(const BuildOrderItem &item) {
	std::tuple<int, int, int> cost = Data::getCost(item);
	if (getAvailMins() >= std::get<0>(cost) && getAvailGas() >= std::get<1>(cost)) {
		return true;
	}
	return false;
}

int ProductionManager::getAvailMins() {
	return bot.getMins() - reservedMins;
}

int ProductionManager::getAvailGas() {
	return bot.getGas() - reservedGas;
}


void ProductionManager::onStart() {
	//["SpawningPool", "Drone", "Overlord", "Drone", "Zergling", "Zergling", "Zergling", "Hatchery", "Zergling", "Zergling", "Zergling", "Zergling", "Zergling"]

	b_queue.queueItem(BuildOrderItem(sc2::UNIT_TYPEID::ZERG_SPAWNINGPOOL, sc2::ABILITY_ID::BUILD_SPAWNINGPOOL, "Spawning Pool", BuildOrderItem::typeOfItem::Building));
	b_queue.queueItem(BuildOrderItem(sc2::UNIT_TYPEID::ZERG_DRONE, sc2::ABILITY_ID::TRAIN_DRONE, "Drone", BuildOrderItem::typeOfItem::Unit));
	b_queue.queueItem(BuildOrderItem(sc2::UNIT_TYPEID::ZERG_DRONE, sc2::ABILITY_ID::TRAIN_DRONE, "Drone", BuildOrderItem::typeOfItem::Unit));
	b_queue.queueItem(BuildOrderItem(sc2::UNIT_TYPEID::ZERG_ZERGLING, sc2::ABILITY_ID::TRAIN_ZERGLING, "Zergling", BuildOrderItem::typeOfItem::Unit));
	b_queue.queueItem(BuildOrderItem(sc2::UNIT_TYPEID::ZERG_ZERGLING, sc2::ABILITY_ID::TRAIN_ZERGLING, "Zergling", BuildOrderItem::typeOfItem::Unit));
	b_queue.queueItem(BuildOrderItem(sc2::UNIT_TYPEID::ZERG_ZERGLING, sc2::ABILITY_ID::TRAIN_ZERGLING, "Zergling", BuildOrderItem::typeOfItem::Unit));
	b_queue.queueItem(BuildOrderItem(sc2::UNIT_TYPEID::ZERG_HATCHERY, sc2::ABILITY_ID::BUILD_HATCHERY, "Hatchery", BuildOrderItem::typeOfItem::Building));
	b_queue.queueItem(BuildOrderItem(sc2::UNIT_TYPEID::ZERG_ZERGLING, sc2::ABILITY_ID::TRAIN_ZERGLING, "Zergling", BuildOrderItem::typeOfItem::Unit));
	b_queue.queueItem(BuildOrderItem(sc2::UNIT_TYPEID::ZERG_ZERGLING, sc2::ABILITY_ID::TRAIN_ZERGLING, "Zergling", BuildOrderItem::typeOfItem::Unit));
	b_queue.queueItem(BuildOrderItem(sc2::UNIT_TYPEID::ZERG_ZERGLING, sc2::ABILITY_ID::TRAIN_ZERGLING, "Zergling", BuildOrderItem::typeOfItem::Unit));
	b_queue.queueItem(BuildOrderItem(sc2::UNIT_TYPEID::ZERG_ZERGLING, sc2::ABILITY_ID::TRAIN_ZERGLING, "Zergling", BuildOrderItem::typeOfItem::Unit));
	b_queue.queueItem(BuildOrderItem(sc2::UNIT_TYPEID::ZERG_ZERGLING, sc2::ABILITY_ID::TRAIN_ZERGLING, "Zergling", BuildOrderItem::typeOfItem::Unit));

	if (nextBuildingLocation == sc2::Point2D(-1, -1)) {
		sc2::ABILITY_ID ability_unit = sc2::ABILITY_ID::BUILD_SPAWNINGPOOL;
		setNextBuildingLocation(ability_unit);
	}

}

void ProductionManager::Overlord() { 
	//b_queue.queueItem_Front(BuildOrderItem(sc2::UNIT_TYPEID::ZERG_OVERLORD, sc2::ABILITY_ID::TRAIN_OVERLORD, "Overlord", BuildOrderItem::typeOfItem::Unit));
	sc2::ABILITY_ID unit_ability = sc2::ABILITY_ID::TRAIN_OVERLORD;
	if (alreadyTrainingUnit(unit_ability)) { return; }
	TryBuildUnit(unit_ability);
}

void ProductionManager::onStep() {
	//TODO: do onFrame for building stuff
	//1 : send drone to build building or larva to make unit
	//2 : if drone dies, re-send & check if enemy now controls that area
	//3 : check if building gets destroyed while building, if under attack maybe cancel when under a threshold???
	//4 : if building under construction and needed for next Item, dont pop item until building finished
	//5 : minerals/gas must be reserved, building is being built and then it tries to queue additional stuff with no resources because it DID have available right before building built

	if (!b_queue.isEmpty()) {
		create(b_queue[0]);
	}

	drawInfo();
}

bool ProductionManager::TryBuildUnit(sc2::ABILITY_ID &unit_ability) {
	//get a larva to build with
	sc2::Units units = bot.getUnits(sc2::Unit::Alliance::Self, sc2::UNIT_TYPEID::ZERG_LARVA);
	for (const auto& unit : units) {
		bot.Actions()->UnitCommand(unit, unit_ability);
		//reservedMins += Data::getCost(unit->unit_type, BuildOrderItem::typeOfItem::Unit).first;
		//reservedGas += Data::getCost(unit->unit_type, BuildOrderItem::typeOfItem::Unit).second;
		return true;
	}

	return false;
}

bool ProductionManager::TryBuildStructure(sc2::ABILITY_ID &unit_ability) {
	//TODO : go through drones, find one thats not carrying anything
	sc2::Units units = bot.getUnits(sc2::Unit::Alliance::Self, sc2::UNIT_TYPEID::ZERG_DRONE);
	for (const auto& unit : units) {
		if (!bot.Observation()->HasCreep(nextBuildingLocation)) {
			setNextBuildingLocation(unit_ability);
		}
		bot.Actions()->UnitCommand(unit, unit_ability, nextBuildingLocation);
		//reservedMins += Data::getCost(unit->unit_type, BuildOrderItem::typeOfItem::Unit).first;
		//reservedGas += Data::getCost(unit->unit_type, BuildOrderItem::typeOfItem::Unit).second;
		return true;
	}
	return false;
}

void ProductionManager::UnreserveResources() {
	//TODO: cycle through all buildings that are building and unreserve the resources we reserved in TryBuildStruction

}

bool ProductionManager::alreadyTrainingUnit(sc2::ABILITY_ID &train_unit) {
	//check if one is already training
	sc2::Units units = bot.getUnits(sc2::Unit::Alliance::Self, sc2::UNIT_TYPEID::ZERG_EGG);
	for (const auto& unit : units) {
		for (const auto& order : unit->orders) {
			if (order.ability_id == train_unit) {
				return true;
			}
		}
	}
	return false;
}

void ProductionManager::drawInfo() {
	std::stringstream ss;
	ss << "Production Queue : \n\n";

	for (auto &unit : bot.getUnits(sc2::Unit::Alliance::Self, sc2::UNIT_TYPEID::ZERG_EGG)) {
		for (const auto& order : unit->orders) {
			ss << sc2::AbilityTypeToName(order.ability_id.operator sc2::ABILITY_ID()) << "\n";
		}
	}
	ss << "\n\nBuild Order Queue : \n\n";

	ss << b_queue.getInfo();

	bot.Debug()->DebugTextOut(ss.str(), sc2::Point2D(0.02f, 0.02f), sc2::Color(255, 255, 0));
}

size_t ProductionManager::CountUnitType(sc2::UNIT_TYPEID unit_type) {
	return bot.Observation()->GetUnits(sc2::Unit::Alliance::Self, sc2::IsUnit(unit_type)).size();
}

void ProductionManager::setNextBuildingLocation(sc2::ABILITY_ID &unit_ability) {
	sc2::Point2D buildLocation = bot.Observation()->GetStartLocation();
	std::vector <sc2::Point2D> buildGrid = getBuildGrid(buildLocation, unit_ability);

	if (buildGrid.empty()) {
		std::cout << "NO BUILD GRID, RANDOMIZING BASED ON BASE START POSITION" << std::endl;
		float rx = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
		float ry = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
		nextBuildingLocation = sc2::Point2D(buildLocation.x + rx * 15.0f, buildLocation.y + ry * 15.0f);
	}
	else {
		//get random one from list of buildGrid entries
		nextBuildingLocation = sc2::GetRandomEntry(buildGrid);
	}

}

std::vector<sc2::Point2D> ProductionManager::getBuildGrid(sc2::Point2D mid, sc2::ABILITY_ID &unit_ability) {
	std::vector<sc2::QueryInterface::PlacementQuery> placeQuery;
	std::vector<sc2::Point2D> buildGrid;

	//first we set the minimum and maximum values for the search area
	float minX = mid.x - 15;
	float minY = mid.y - 15;
	float maxX = mid.x + 15;
	float maxY = mid.y + 15;

	if (minX < 0) { minX = 0;  }
	if (minY < 0) { minY = 0;  }
	if (maxX > bot.Observation()->GetGameInfo().width) {
		maxX = float(bot.Observation()->GetGameInfo().width);
	}
	if (maxY > bot.Observation()->GetGameInfo().width) {
		maxY = float(bot.Observation()->GetGameInfo().width);
	}
		//load up the vector with a query for each point
		for (float x = minX; x <= maxX; x++) {
			for (float y = minY; y <= maxY; y++) {
				placeQuery.push_back(sc2::QueryInterface::PlacementQuery(unit_ability, sc2::Point2D(x, y)));
				buildGrid.push_back(sc2::Point2D(x, y));
			}
		}

		//query each position
		std::vector<bool> output = bot.Query()->Placement(placeQuery);

		if (!output.empty()) {
			//if a point is unbuildable, set its location to 0:0 so we know it doesn't work
			for (int i = 0; i != output.size() - 1; ++i) {
				//std::cerr << i << ": " << output[i] << std::endl;
				if (output[i] == false) {
					buildGrid[i] = sc2::Point2D(0, 0);
				}
			}
		}
		//go through the vector and remove all the unbuildable (0:0) points
		for (std::vector<sc2::Point2D>::iterator point = buildGrid.begin(); point != buildGrid.end();) {
			if (*point == sc2::Point2D(0, 0)) {
				point = buildGrid.erase(point);
			} else {
				++point;
			}
		}
		for (std::vector<sc2::Point2D>::iterator point = buildGrid.begin(); point != buildGrid.end();) {
			bool tooClose = false;

			//TODO: check if close to other buildings

			//TODO: check if blocking min/gas spots

			//remove if it has no creep to build on
			if (!bot.Observation()->HasCreep(*point)) {
				point = buildGrid.erase(point);
			} else {
				++point;
			}
		}
		//DOESNT DEBUG BOX WORK WHY?????
		printBuildGrid(buildGrid);
		return buildGrid;
}

void ProductionManager::printBuildGrid(std::vector<sc2::Point2D> buildGrid) {
	for (auto point : buildGrid) {
		sc2::Point3D box = sc2::Point3D(point.x, point.y, bot.Observation()->GetStartLocation().z + 1);
		bot.Debug()->DebugBoxOut(box, sc2::Point3D(box.x + 1, box.y + 1, box.z), sc2::Colors::White);
	}
}