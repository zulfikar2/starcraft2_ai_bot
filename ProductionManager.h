#pragma once

#include "BuildOrder.h"

class PacifistAres;

class ProductionManager {
	PacifistAres &bot;
	BuildOrderQueue b_queue;

	bool canMake(const BuildOrderItem &item);
	void setBuildOrder(const BuildOrderQueue &b_queue);
	void create(BuildOrderItem &item);

	int getAvailMins();
	int getAvailGas();

	int reservedMins, reservedGas;

	bool TryBuildStructure(sc2::ABILITY_ID &unit_ability);
	bool TryBuildUnit(sc2::ABILITY_ID &unit_ability);
	void Overlord();
	void UnreserveResources();

	bool checkBuilt(sc2::UNIT_TYPEID &unit_type);

	bool alreadyTrainingUnit(sc2::ABILITY_ID &train_unit);
	bool hasRequired(sc2::UNIT_TYPEID &unit_type);
	size_t CountUnitType(sc2::UNIT_TYPEID unit_type);

	void setNextBuildingLocation(sc2::ABILITY_ID &unit_ability);
	std::vector<sc2::Point2D> getBuildGrid(sc2::Point2D pt, sc2::ABILITY_ID &unit_ability);

	sc2::Point2D nextBuildingLocation;
	void printBuildGrid(std::vector<sc2::Point2D> buildGrid);

public:

	ProductionManager(PacifistAres &bot);

	void onStart();
	void onStep();
	void drawInfo();
};
