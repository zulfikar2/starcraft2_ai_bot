# Starcraft2_AI_Bot

Using sc2api-client found here : https://github.com/Blizzard/s2client-api by Blizzard, I create a bot that plays the game and learns. The bot will be focused on playing as a zerg player.

# License

Copyright (c) 2019 Zulfikar Yusufali

Licensed under the [MIT license](https://gitlab.com/zulfikar2/starcraft2_ai_bot/blob/master/LICENSE).